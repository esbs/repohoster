#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

# CGI output must start with an least empty line, optionally lead by headers
echo

set -eu

_base_path="$(readlink -f "${SCRIPT_FILENAME%${SCRIPT_NAME}}")"
_dir="$(readlink -f "${_base_path}/${REQUEST_URI%?${QUERY_STRING}}")"
_title="Index of '${REQUEST_URI}'"

printf "<html>\n\t<head>\n\t\t<title>%s</title>\n\t</head>\n\t<body>\n\t\t<h1>%s</h1>\n\t\t<hr>\n\t\t<pre>\n" \
       "${_title}" "${_title}"
for _file in "${_dir}/".* "${_dir}/"*; do
	_filename="$(basename "${_file}")"
	if [ ! -e "${_file}" ] || \
	   [ "${_file##${_base_path}}" = "/.." ] || \
	   [ "${_filename}" = ".git" ] || \
	   [ "${_filename}" = "cgi-bin" ]; then
		continue
	fi
	_url="<a href=\"${_filename}\">${_filename}</a>"
	ls -adFl "${_file}" | sed "s|${_file}|${_url}|g"
done
printf "\t\t</pre>\n\t\t<hr>\n\t</body>\n</html>\n"

exit 0
