#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_WWW_ROOT='/usr/share/webapps/repohoster'

_msg()
{
	_level="${1:?'Missing argument to function'}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

usage()
{
	echo 'Usage: ${0} [OPTIONS]'
	echo 'Clone package repository and host it'
	echo "    -b  Base path to serve (default: '${DEF_WWW_ROOT}') [WWW_ROOT]"
	echo '    -f  Force local directory (try not to git clone)'
	echo '    -h  Print usage'
	echo '    -r  Location of the package repository [PACKAGE_REPOSITORY]'
	echo
	echo 'All options can also be passed in environment variables (listed between [brackets]).'
	echo 'Warning: Hosting a local directory will try to create a cgi-bin/index.cgi in the directory.'
}

main()
{
	_start_time="$(date '+%s')"

	while getopts ':b:fhr:' _options; do
		case "${_options}" in
		b)
			www_root="${OPTARG}"
			;;
		f)
			force='true'
			;;
		h)
			usage
			exit 0
			;;
		r)
			package_repository="${OPTARG}"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	package_repository="${package_repository:-"${PACKAGE_REPOSITORY:-}"}"
	www_root="${www_root:-"${WWW_ROOT:-"${DEF_WWW_ROOT}"}"}"

	src_file="$(readlink -f "${0}")"
	src_dir="${src_file%%"${src_file##*'/'}"}"

	"${src_dir}/buildenv_check.sh"

	if [ -z "${package_repository:-}" ]; then
		e_warn "Required parameter 'package_repository' not set, running with pwd"
		package_repository="$(pwd)"
	fi

	if [ -e "${package_repository:-}" ]; then
		if [ -L "${package_repository}" ]; then
			package_repository="$(readlink -f "${package_repository}")"
		fi
		if [ "${package_repository#'/'*}" = "${package_repository}" ]; then
			package_repository="$(pwd)/${package_repository}"
		fi
	elif echo "${package_repository:-}" | \
	     grep -q '.*://.*'; then
		if [ "${force:-}" = 'true' ]; then
			e_warn 'The force flag `-f` cannot be used with URIs'
			force='false'
		fi
	else
		e_err "The repository '${package_repository:-}' is not a file and does not look like an URI."
		exit 1
	fi

	if [ "${force:-}" = 'true' ] || \
	   ! "${src_dir}/bin/reposync.sh" -r "${package_repository}" "${www_root}"; then
		www_root="${package_repository}"
	fi
	WWW_ROOT="${www_root}" "${src_dir}/bin/repohoster.sh"

	echo "Ran application for $(($(date '+%s') - _start_time)) seconds"
}

main "${@}"

exit 0
